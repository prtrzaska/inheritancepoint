#include "block.h"


Block::Block(float x, float y, float z, float w, float h, float d)
{
    xPos = x;
    yPos = y;
    zPos = z;
    box.setWidth(w);
    box.setHeight(h);
    box.setDepth(d);
}

void Block::draw()
{
    ofPushMatrix();
    ofTranslate(xPos, yPos, zPos);
    if (texture.getWidth() > 0){// we have a texture
        texture.getTexture().bind();
        box.draw();
        texture.getTexture().unbind();
    }
    else{
        box.draw();
    }
    ofPopMatrix();
}

void Block::setTexture(ofImage& _texture)
{
    texture = _texture;
}


float Block::getXPos()
{
    return xPos;
}
float Block::getYPos()
{
    return yPos;
}
float Block::getZPos()
{
    return zPos;
}

