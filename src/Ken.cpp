#include "Ken.h"

Ken::Ken(float x, float y, float z, float w, float h, float d) : Block(x, y, z, w, h, d)
{
	//500, 500, 100, 50, 50, 50
	wheel1.setRadius(h / 2);
	wheel1.setPosition(0, 0, 0);

	
	wh.setPosition(100, 0, 0);
	wh.setRadius(h / 2);

	stone.set(80, 80, 40);
	stone.setPosition(-50, 100, 0);

	chassis.set(w / 15, 200);
	chassis.setPosition(0, h, 0);

	chassis2.setBottomCapColor(ofColor(255));
	chassis2.set(w / 15, 50);
	chassis2.setPosition(-35, -10, 0);

	chassis3.setBottomCapColor(ofColor(255));
	chassis3.set(w / 15, 130);
	chassis3.setPosition(-31, 45, 0);

	bodyMat.setDiffuseColor(ofFloatColor(1.0,0,1.0));
	stalkMat.setDiffuseColor(ofFloatColor(0,1.0,1.0));
}
void Ken::draw()
{
	// store coordinate system
	ofPushMatrix();
	// translate coordinate 0,0,0 to the position of the block
	ofTranslate(xPos, yPos, zPos);
	// wrap the foliage in the foliage material and draw it
	bodyMat.begin();
	wheel1.draw();
	wh.draw();
	bodyMat.end();
	ofTranslate(100, -chassis.getY()*0.5, 0);
	// now wrap and draw the trunk
	ofRotateDeg(90);
	stalkMat.begin();
	chassis.draw();
	ofRotateDeg(45);
	chassis2.draw();
	ofRotateDeg(-45);
	chassis3.draw();
	stalkMat.end();
	texture.getTexture().bind();
	stone.draw();
	texture.getTexture().unbind();
	// return 0,0,0 to its normal place!
	ofPopMatrix();
}