#pragma once

#include "ofMain.h"
#include "positionmarker.h"
#include "block.h"
#include "treeblock.h"
#include "candyCane.h"
#include "buzz.h"
#include "coolBlock.h"
#include "Apple.h"
#include "Ken.h"
#include "planetBlock.h"
#include <vector>

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		~ofApp();
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
	private:
        ofLight light;
        float blockSize = 50;

        PositionMarker marker{blockSize};

        std::vector <Block*> blocks;

        ofImage blockTexture{"grass.jpg"}; //standard texture grass
		ofImage candyTexture{ "candy.jpg" }; //Eliza Custom block Candy -->forum-->https://learn.gold.ac.uk/mod/forum/discuss.php?d=210636#p311421
		ofImage cool{ "alientexture.png" }; // Mario antropomorphizm --> https://learn.gold.ac.uk/mod/forum/discuss.php?d=210636#p311387
		ofImage stones{ "stone.jpg" }; //texture for stones
		ofImage planetTexture{ "planet.jpg" };// texture for planet -->https://learn.gold.ac.uk/mod/forum/discuss.php?d=210636#p311609
		TreeBlock tree{ 500, 500, 100, 50, 50, 50 }; //extended tree block
		buzz fh{ 500, 500, 100, 50, 50, 50 }; // buzz block with random translation -->https://learn.gold.ac.uk/mod/forum/discuss.php?d=210636#p311399
		Apple apple{ 500, 500, 100, 50, 50, 50 }; //Megan apple -->https://learn.gold.ac.uk/mod/forum/discuss.php?d=210636#p311371

		Ken kennycar{ 500, 500, 100, 50, 50, 50 }; //kennys car
		
};
