#pragma once
#include "ofMain.h"
#include "block.h"

class Ken : public Block {
	public:
	
		Ken(float x, float y, float z, float w, float h, float d);
		
			virtual void draw();

	private:
		ofSpherePrimitive wheel1;
		ofSpherePrimitive wh;
		ofBoxPrimitive stone;
		ofCylinderPrimitive chassis;
		ofCylinderPrimitive chassis2;
		ofCylinderPrimitive chassis3;
		ofMaterial bodyMat;
		ofMaterial stalkMat;
};