//  
//  candyCane.cpp
//  PAP LAB  9 BLOCKWORLD 2.0
//
//  Created by Eliza Szilagyi. on 15/12/2018 
//   

#include "candyCane.h"

candyCane::candyCane(float x, float y, float z, float w, float h, float d)
: Block(x, y, z, w, h, d)
{
	top.setParent(mid);
    mid.set(20,100,5);
    mid.setPosition(0,0,0);
    top.set(50,20,5);
    top.setPosition( 16, -58.5, 0);
    down.set(20,20,5);
    down.setPosition(30,-40, 0);
    
    std::cout << "candy constructed " << std::endl;
}


void candyCane::draw()
{

    ofPushMatrix();
        texture.getTexture().bind();
        ofTranslate(xPos,yPos,zPos);
        mid.draw();
        top.draw();
		down.draw();
        texture.getTexture().unbind();
		// ofRotateDeg(20)
    ofPopMatrix();
}
