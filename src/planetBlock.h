#ifndef PLANETBLOCK_H
#define PLANETBLOCK_H

#include "block.h"
#include "ofMain.h"

class PlanetBlock : public Block {
public:
	PlanetBlock(float x, float y, float z, float w, float h, float d);
	~PlanetBlock();
	virtual void draw();
private:
	ofBoxPrimitive pBody;
	ofBoxPrimitive mBody;
	ofMaterial planetMat;
	ofMaterial moonMat;
	float mOrb = 0.0;
};
#endif // TREEBLOCK_H