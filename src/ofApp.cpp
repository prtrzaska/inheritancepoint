#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetBackgroundColor(0);
	ofEnableDepthTest();
	ofEnableLighting();
	ofEnableNormalizedTexCoords();
	light.setPointLight();
	light.setDiffuseColor(ofFloatColor(1.0, 1.0, 1.0));
	light.setAmbientColor(ofFloatColor(0.3, 0.3, 0.3));
	light.setPosition(ofGetWidth()*.5, ofGetHeight()*.5, 500);

}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
    light.enable();
    for (Block* b : blocks){
        b->draw();
    }

    marker.draw();
    light.disable();
}

ofApp::~ofApp()
{
	std::cout << "destroying the blocks" << std::endl;
	for (Block* b : blocks) {
		delete b; //give memory back
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'b') 
	{
		Block* b = new Block{ marker.getXPos(),
				marker.getYPos(),
				marker.getZPos(),
				blockSize, blockSize, blockSize };
		b->setTexture(blockTexture);
		blocks.push_back(b);
	}
	if (key == 't') 
	{
		TreeBlock* t = new TreeBlock{ marker.getXPos(),
				marker.getYPos(),
				marker.getZPos(),
				blockSize, blockSize, blockSize };
		
		blocks.push_back(t);
	}
	if (key == 'c')
	{
		candyCane* c = new candyCane{ marker.getXPos(),
			marker.getYPos(),
			marker.getZPos(),
			blockSize, blockSize, blockSize };
			c->setTexture(candyTexture);
			blocks.push_back(c);
	}
	if (key == 'f')
	{
		buzz* f = new buzz{ marker.getXPos(),
			marker.getYPos(),
			marker.getZPos(),
			blockSize, blockSize, blockSize };
		
		blocks.push_back(f);
	}
	if (key == 'h') 
	{
		coolBlock* h = new coolBlock{ marker.getXPos(),

			marker.getYPos(),

			marker.getZPos(),

			blockSize, blockSize, blockSize };

		h->setTexture(cool);

		blocks.push_back(h);

	}
	if (key == 'a')
	{
		Apple* a = new Apple{ marker.getXPos(),

			marker.getYPos(),

			marker.getZPos(),

			blockSize, blockSize, blockSize };

		blocks.push_back(a);

	}
	if (key == 'k')
	{
		Ken* k = new Ken{ marker.getXPos(),
			marker.getYPos(),
			marker.getZPos(),
			blockSize, blockSize, blockSize };
		k->setTexture(stones);
		blocks.push_back(k);
	}
	if (key == 'p')
	{

		PlanetBlock* p = new PlanetBlock{ marker.getXPos(),

				marker.getYPos(),

				marker.getZPos(),

				blockSize, blockSize, blockSize };

		p->setTexture(planetTexture);

		blocks.push_back(p);

	}



	marker.moveAboveBlocks(blocks);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    marker.setMousePosition(x, y, ofGetWidth(), ofGetHeight());
    marker.moveAboveBlocks(blocks);
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    Block* b = new Block{marker.getXPos(),
            marker.getYPos(),
            marker.getZPos(),
            blockSize, blockSize, blockSize};
    b->setTexture(blockTexture);
    blocks.push_back(b);
    marker.moveAboveBlocks(blocks);

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
