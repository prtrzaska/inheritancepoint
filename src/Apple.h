#pragma once
#include "ofMain.h"
#include "block.h"
class Apple : public Block {
public:
	Apple(float x, float y, float z, float w, float h, float d);
	
	virtual void draw();

private:
	ofSpherePrimitive body;
	ofCylinderPrimitive stalk;
	ofMaterial bodyMat;
	ofMaterial stalkMat;
};

