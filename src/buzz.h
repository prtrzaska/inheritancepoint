#pragma once

#ifndef buzz_h
#define buzz_h

#include "block.h"
#include "ofMain.h"

class buzz : public Block
{
public:
	buzz(float x, float y, float z, float w, float h, float d);
	virtual void draw();
	

private:
	ofSpherePrimitive theSphere;
	ofBoxPrimitive top;
	ofMaterial topMat;
	ofMaterial theSphereMat;
	
};

#endif
