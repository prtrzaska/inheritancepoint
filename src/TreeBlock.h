#ifndef TREEBLOCK_H
#define TREEBLOCK_H

#include "block.h"
#include "ofMain.h"

class TreeBlock : public Block {
public:
  TreeBlock(float x, float y, float z, float w, float h, float d);
  virtual void draw();
private:
    ofConePrimitive foliage;
    ofCylinderPrimitive trunk;
    ofMaterial foliageMaterial;
    ofMaterial trunkMaterial;
};
#endif // TREEBLOCK_H