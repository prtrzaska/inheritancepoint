//
//  coolBlock.hpp
//  PAPLab_9
//
//  Created by Mario A. Sánchez on 14/12/2018.
//

#ifndef coolBlock_h
#define coolBlock_h

#include <stdio.h>
#include "block.h"
#include "ofMain.h"

class coolBlock : public Block {
public:
    coolBlock(float x, float y, float z, float w, float h, float d);
    virtual void draw();
private:
    ofBoxPrimitive body;
    ofBoxPrimitive legOne;
    ofBoxPrimitive legTwo;
    ofBoxPrimitive handOne;
    ofBoxPrimitive handTwo;
    ofSpherePrimitive head;
};
#endif /* coolBlock_hpp */
