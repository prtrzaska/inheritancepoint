#ifndef BLOCK_H
#define BLOCK_H

#include "ofMain.h"


class Block
{
	public:
		Block(float x, float y, float z, float w, float h, float d);
			virtual void draw();
					void setTexture(ofImage& _texture);
					float getXPos();
					float getYPos();
					float getZPos();
	protected: //visible from TreeBlock
					float xPos;
					float yPos;
					float zPos;
					ofImage texture;
					ofBoxPrimitive box;
};

#endif // BLOCK_H
