#include "positionmarker.h"

PositionMarker::PositionMarker(float _blockSize)
{
    xPos = 250;
    yPos = 500;
    zPos = 250;
    blockSize = _blockSize;
    box.setWidth(blockSize);
    box.setDepth(blockSize);
    box.setHeight(blockSize);
    material.setDiffuseColor(ofFloatColor(0, 255, 0));
}

void PositionMarker::setMousePosition(float mouseX, float mouseY, float width, float height)
{
    xPos = mouseX - fmod(mouseX, blockSize);
    zPos = ((height - mouseY) + fmod(mouseY, blockSize)) - 400;

}
void PositionMarker::moveAboveBlocks(std::vector<Block*> blocks)
{
    yPos = 600;
    float highest = 600;
    for (int i=0; i<blocks.size(); i++){
        if (blocks[i]->getXPos() == xPos && blocks[i]->getZPos() == zPos){
            // in the same position
            if (blocks[i]->getYPos() < highest){
                highest = blocks[i]->getYPos();
            }
            //break;
        }
    }
//    for (Block b : blocks){
//        if (b.getXPos() == xPos && b.getZPos() == zPos){
//            // in the same position
//            if (b.getYPos() < highest){
//                highest = b.getYPos();
//            }
//            //break;
//        }
//    }
    yPos = highest - blockSize;

}
void PositionMarker::draw()
{
    ofPushMatrix();
    ofTranslate(xPos, yPos, zPos);
    material.begin();
    box.draw();
    material.end();
    ofPopMatrix();
}

float PositionMarker::getXPos()
{
    return xPos;
}
float PositionMarker::getYPos()
{
    return yPos;
}
float PositionMarker::getZPos()
{
    return zPos;
}

