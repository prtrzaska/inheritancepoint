//  
//  candyCane.cpp
//  PAP LAB  9 BLOCKWORLD 2.0
//
//  Created by Eliza Szilagyi. on 15/12/2018 
// 

#ifndef candyCane_h
#define candyCane_h

#include <stdio.h>
#include "block.h"
#include "ofMain.h"

class candyCane : public Block {
public:
    candyCane(float x, float y, float z, float w, float h, float d);
    virtual void draw();
private:
    ofBoxPrimitive mid;
    ofBoxPrimitive top;
    ofBoxPrimitive down;
	
};
#endif 
