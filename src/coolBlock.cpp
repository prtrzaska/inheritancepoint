//
//  coolBlock.cpp
//  PAPLab_9
//
//  Created by Mario A. Sánchez on 14/12/2018.
//

#include "coolBlock.h"

coolBlock::coolBlock(float x, float y, float z, float w, float h, float d)
: Block(x, y, z, w, h, d)
{
    
    body.set(20,40,10);
    body.setPosition(0,0,0);
    
    head.setPosition(0, -40,0);
    head.setRadius(10);
    
    legOne.set(9,40,10);
    legOne.setPosition(-5.4, 30, 0);
    legTwo.set(9,40,10);
    legTwo.setPosition(5.4,30, 0);
    
    handOne.set(9,40,10);
    handOne.setPosition(-15.6,29,0);
    handTwo.set(9,40,10);
    handTwo.setPosition(15.6,29,0);
    
    
    
    std::cout << "coolBlock constructed " << std::endl;
}


void coolBlock::draw()
{

    
    ofPushMatrix();
    if (texture.getWidth() > 0){// we have a texture
        texture.getTexture().bind();
        ofTranslate(xPos,yPos,zPos);
        //Body
        body.draw();
        //Head
        head.draw();
        //Legs
		
        legOne.draw();
		
        legTwo.draw();
		
        //Hand One
        ofPushMatrix();
		ofRotateDeg(15);
        handOne.draw();
        ofPopMatrix();
        //Hand Two
        ofPushMatrix();
		ofRotateDeg(-25);
        handTwo.draw();
        ofPopMatrix();
        texture.getTexture().unbind();
    }
    else{
        ofTranslate(xPos,yPos,zPos);
        //Body
        body.draw();
        //Head
        head.draw();
        //Legs
        legOne.draw();
		//ofRotateDeg(-35);
        legTwo.draw();
        //Hand One
        ofPushMatrix();
        //ofRotateDeg(45);
        handOne.draw();
        ofPopMatrix();
        //Hand Two
        ofPushMatrix();
        //ofRotateDeg(25);
        handTwo.draw();
        ofPopMatrix();
    }
    
    
    ofPopMatrix();
}
