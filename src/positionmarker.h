#ifndef POSITIONMARKER_H
#define POSITIONMARKER_H

#include <vector>

#include "ofMain.h"
#include "block.h"

class PositionMarker
{
public:
    PositionMarker(float blockSize);
    void setMousePosition(float mouseX, float mouseY, float w, float h);
    void draw();
    float getXPos();
    float getYPos();
    float getZPos();
    void moveAboveBlocks(std::vector <Block*> blocks);
private:
    ofMaterial material;
    ofBoxPrimitive box;
    float xPos;
    float yPos;
    float zPos;
    float blockSize;
};

#endif // POSITIONMARKER_H
