#include "treeblock.h"

TreeBlock::TreeBlock(float x, float y, float z, float w, float h, float d)
    : Block(x, y, z, w, h, d)
{
	foliage.setHeight(h / 2);
	foliage.setRadius(w / 2);
	std::cout << "TreeBlock constructed " << std::endl;
	trunk.setHeight(h / 2);
	trunk.setRadius(w / 4);
	trunk.setPosition(0, h / 4, 0);

	foliageMaterial.setDiffuseColor(ofFloatColor(0.8, 0.2, 0.2));
	trunkMaterial.setDiffuseColor(ofFloatColor(1.0, 1.0, 0));
}

void TreeBlock::draw()
{
	// store coordinate system
	ofPushMatrix();
	// translate coordinate 0,0,0 to the position of the block
	ofTranslate(getXPos(), getYPos(), getZPos());
	// wrap the foliage in the foliage material and draw it
	foliageMaterial.begin();
	foliage.draw();
	foliageMaterial.end();
	// translate 0,0,0 to slightly lower
	ofTranslate(0, trunk.getY(), 0);
	// now wrap and draw the trunk
	trunkMaterial.begin();
	trunk.draw();
	trunkMaterial.end();
	// return 0,0,0 to its normal place!
	ofPopMatrix();
}