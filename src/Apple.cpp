#include "Apple.h"

Apple::Apple(float x, float y, float z, float w, float h, float d) : Block(x, y, z, w, h, d)
{
	//500, 500, 100, 50, 50, 50
	body.setRadius(h / 2);

	stalk.set(w / 15, h/2);
	
	stalk.setPosition(0, h, 0);

	bodyMat.setDiffuseColor(ofFloatColor(1.0, 0, 0));
	stalkMat.setDiffuseColor(ofFloatColor(1.0, 1.0, 0));
}

/*Apple::~Apple()
{
}*/

void Apple::draw()
{
	// store coordinate system
	ofPushMatrix();
	// translate coordinate 0,0,0 to the position of the block
	ofTranslate(xPos, yPos, zPos);
	// wrap the foliage in the foliage material and draw it
	bodyMat.begin();
	body.draw();
	bodyMat.end();
	// translate 0,0,0 to slightly lower
	ofTranslate(0, -stalk.getY()*1.5, 0);
	// now wrap and draw the trunk
	stalkMat.begin();
	stalk.draw();
	stalkMat.end();
	// return 0,0,0 to its normal place!
	ofPopMatrix();
}
