
#include "buzz.h"
#include <cmath>

buzz::buzz(float x, float y, float z, float w, float h, float d)
	: Block(x, y, z, w, h, d)
{
	top.setHeight(h);
	top.setWidth(w);
	top.setDepth(z);

	top.setHeight(h / 8);
	top.setWidth(w / 2);
	
	topMat.setDiffuseColor(ofFloatColor(0.8, 1.0, 0.5,0.4));
	theSphereMat.setDiffuseColor(ofFloatColor(0.8, 0.5, 1.0,0.8));
	ofSetFrameRate(20);

}

void buzz::draw()
{
	ofPushMatrix();
	//make the random for animation
	ofTranslate(ofRandom(xPos -50, xPos + 50), ofRandom(yPos - 50, yPos + 50), zPos);
	
	//wrap around their set colors
	topMat.begin();
	//draw top and sphere from the same center, so the top goes through the sphere
	top.draw();
	topMat.end();

	theSphereMat.begin();
	theSphere.draw();
	theSphereMat.end();

	ofPopMatrix();
}


