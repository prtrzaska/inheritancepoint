#include "planetBlock.h"

PlanetBlock::PlanetBlock(float x, float y, float z, float w, float h, float d)
	: Block(x, y, z, w, h, d) 
{
	pBody.setWidth(w/2); // Setting Planet Dimensions
	pBody.setHeight(h/2);
	pBody.setDepth(d/2);
	 
	mBody.setWidth(w / 8); // Setting Moon Dimensions
	mBody.setHeight(h / 8);
	mBody.setDepth(d / 8);

	planetMat.setDiffuseColor(ofColor(0, 0, 200)); // Setting planet material colour
	moonMat.setDiffuseColor(ofColor(150, 150, 150)); // Setting planet material colour

	mOrb = rand() % 360; // Setting initial position in orbit

	std::cout << "PlanetBlock constructed " << std::endl;
}

PlanetBlock::~PlanetBlock() {
	std::cout << "PlanetBlock destroyed " << std::endl;
}

void PlanetBlock::draw() {

	mOrb = fmod(mOrb + 0.25, 360); // Incrimenting orbit position
	ofPushMatrix(); 

	ofTranslate(getXPos(), getYPos(), getZPos());

	ofPushMatrix();
	ofRotate(45, 0.5, 0.5, 0); // tilting planet
	pBody.rotate(1, 0.0, 1.0, 0.0); // Rotating planet

	// Drawing planet
	if (texture.getWidth() > 0) {// Checking if planet has a texture
		texture.getTexture().bind();
		pBody.draw();
		texture.getTexture().unbind();
	}
	else {
		planetMat.begin();
		pBody.draw();
		planetMat.end();
	}
	
	ofPopMatrix();

	ofPushMatrix();

	// Positioning moon in orbit
	ofRotate(mOrb, 0.0, 1.0, 0.0); 
	ofTranslate(pBody.getWidth(), 0, 0);

	// Drawing moon
	moonMat.begin();
	mBody.draw();
	moonMat.end();
	ofPopMatrix();

	ofPopMatrix();

}